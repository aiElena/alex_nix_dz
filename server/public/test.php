<?php
//сортировка в обратном порядке
//а потом берем второй элемент
//по номеру 1

//* 3. слот про машину
function slotMachine($arr): bool
{
    if (($arr[0] == $arr[1]) && ($arr[1] == $arr[2])) {
        return true;
    } else {
        return false;
    }
}

//* 4. Сэм и фродо
function solution(array $arr): bool
{
    $sam = array_search('Sam', $arr);
    $frodo = array_search('Frodo', $arr);
    return abs($sam - $frodo) == 1;
}

//* 5. Найдите второе наибольшее число в массиве
function max(array $arr): int
{
    rsort($arr, SORT_NUMERIC);
    return $arr[1];
}

//* 6. массив, содержащий строки, длины которых соответствуют наидлиннейшей строке
function str($arr): array
{
    usort($arr, function ($a, $b) {
        return strlen($b) - strlen($a);
    });
    $r = [];
    foreach ($arr as $v) {
        if (strlen($v) == strlen($arr[0])) {
            $r[] = $v;
        }
    }
    return $r;
}

//* 7. финальная оценка студента по пяти предметам
function grade(array $a): string
{
    $average = array_sum($a) / count($a);
    if ($average > 90) return "Grade: A";
    if ($average > 80) return "Grade: B";
    if ($average > 60) return "Grade: D";
    return "Grade: F";
}

//* 8. название фигуры
function figure(int $n): string
{
    switch ($n) {
        case 1 :
            return "circle";
        case 2 :
            return "semi-circle";
        case 3:
            return "triangle";
        case 4:
            return "square";
        case 5:
            return "pentagon";
        case 6 :
            return "hexagon";
        case 7:
            return "heptagon";
        case 8:
            return "octagon";
        case 9:
            return "nonagon";
        case 10 :
            return "decagon";
    }
}

/**
 * 9. сколько ног у всех животных
 * @param int $a
 * @param int $b
 * @param int $c
 * @return int
 *
 *

 * 9. сколько ног у всех жиai.alexandrova@ukr.net/
 *
 *
 *  * 9. сколько ног у всех животных/ai.alexandrova@ukr.netunction legs(int $a, int $b, int $c): int
    {
        return $a * 2 + $b * 4 + $c * 4;
    }

//* 10. функция, которая трансформирует массив слов в массив длин этих слов
    function long(array $arr): array
    {
        return array_map(function ($v) {
            return strlen($v);
        }, $arr);
    }

//* 11. проверка множественного числа
    function lot(string $s): bool
    {
        $l = substr($s, strlen($s) - 1, 1);
        return $l == "s";
    }


//@author Elena Alexandrova <ai.alexandrova@ukr.net>