<?php
session_start();
echo "**";
echo '<h2>1 Простые числа</h2>';

for ($i = 2; $i < 100; $i++) {
    for ($j = 2; $j < $i; $j++) {
        if (($i % $j) != 0) {
            continue;
        } else {
            $num = true;
            break;
        }
    }
    if (!$num) echo "$i" . " ";
    $num = false;
}

echo '<h2>2 Случайные числа</h2>';

$a = array();
while (count($a) < 100) {
    $random = mt_rand(1, 100);
    if ($random % 2 == 0) {
        echo $a[] = $random . " ";
    }
}

echo '<h2>3 Случайные числа (1 - 5)</h2>';

$a = array();
while (count($a) < 20) {
    $random = mt_rand(1, 5);
    echo $a[] = $random . " ";
}
echo "<hr>";
$cnt = array_fill(1, 5, 0);
foreach ($a as $num) {
    $cnt[$num]++;
}

echo "<pre>";
print_r($cnt);
echo "</pre>";

echo "<h2>4 Таблица</h2>";

$rows = 3;
$cols = 5;
$table = "<table border = '1' width = '50%'>";
$count = 0;

for ($i = 1; $i <= $rows; $i++) {
    $table .= "<tr align = 'center'>";
    for ($j = 1; $j <= $cols; $j++) {
        $count++;
        if ($count == 1 || $count == 5 || $count == 12) {
            $table .= "<td bgcolor = 'red'>$count</td>";
        } else {
            $table .= "<td>$count</td>";
        }
    }
    $table .= "</tr>";
}
$table .= "</table>";
echo $table;

echo "<h2>5 Определить пору года</h2>";

$month = mt_rand(1, 12);
echo $month . " месяц - ";
if ($month == 12 || $month == 1 || $month == 2) {
    $rezult = 'Зима';
}
if ($month > 2 && $month <= 5) {
    $rezult = 'Весна';
}
if ($month > 5 && $month <= 8) {
    $rezult = 'Лето';
}
if ($month > 8 && $month <= 11) {
    $rezult = 'Осень';
}
echo $rezult;


echo "<h2>6 Первый символ из строки</h2>";

$str = 'abcde';
$first = $str[0];
if ($first == 'a') {
    echo "да";
} else {
    echo "нет";
}

echo "<h2>7 Строка с цифрами</h2>";

$str = '12345';
if ($str[0] = '1' || $str[1] = '2' || $str[2] = '3') {
    echo "да";
} else {
    echo "нет";
}

echo "<h2>8 Переменная test</h2>";

$test = true;
if ($test == true) {
    echo "да";
} else {
    echo "нет";
}
echo "<hr>";
$test = false ? 'да' : 'нет';
echo $test;

echo "<h2>9 Определение языка</h2>";

$ru = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
$en = ['mn', 'ts', 'wd', 'th', 'fr', 'st', 'sn'];
$lang = 'ru';
if ($lang == 'ru') {
    echo "<pre>";
    print_r($ru);
    echo "</pre>";
} else {
    echo "<pre>";
    print_r($en);
    echo "</pre>";
}
echo "<hr>";
$lang == 'en' ? print_r($ru) : print_r($en);

echo "<h2>10 Определить четверть часа</h2>";

$clock = mt_rand(0, 59);
{
    echo $clock;
    echo "<hr>";
    if ($clock >= 0 && $clock <= 15) {
        echo "Первая четверть";
    }
    if ($clock > 15 && $clock <= 30) {
        echo "Вторая четверть";
    }
    if ($clock > 30 && $clock <= 45) {
        echo "Третья четверть";
    }
    if ($clock > 45 && $clock <= 59)
        echo "Четвертая четверть";
}
echo "<hr>";

$cl = mt_rand(0, 59);
echo $cl;
echo "<hr>";
//echo ($cl >= 0 && $cl <= 15) ? 'Первая четверть' : ($cl > 15 && $cl <= 30) ? 'Вторая четверть' : ($cl > 30 && $cl <= 45) ? 'Третья четверть' : ($cl > 45 && $cl <= 59) ? 'Четвертая четверть' : '??????';
//$cl = 3;
//echo ($cl == 1) ? "one" : ($cl == 2) ? "two" : "three";
echo "<hr>";

echo "***";
echo "<h2>1 Своя функция count()</h2>";

echo "Вы обновили страницу " . $_SESSION["counter"]++ . " раз";

echo "<h2>2 Развернуть массив</h2>";

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
echo "<pre>";
print_r($arr);
echo "</pre>";

$arr = array_reverse($arr);

echo "<pre>";
print_r($arr);
echo "</pre>";

echo "<h2>3 Развернуть массив</h2>";

$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
echo "<pre>";
print_r($arr);
echo "</pre>";

$arr = array_reverse($arr);

echo "<pre>";
print_r($arr);
echo "</pre>";

echo "<h2>4 Развернуть строку</h2>";

$str = 'Hi I am Alex';
echo $str;
$str = strrev($str);
echo "<br>";
echo $str;


echo "<h2>5 Все буквы маленькие</h2>";

$str = 'Hi I am Alex';
$str = strtolower($str);
echo $str;

echo "<h2>6 Все буквы большие</h2>";

$str = 'Hi I am Alex';
$str = strtoupper($str);
echo $str;

echo "<h2>7 Все буквы маленькие</h2>";

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arr = array_map('strtolower', $arr);
echo "<pre>";
print_r($arr);
echo "<pre>";

echo "<h2>8 Все буквы большие</h2>";

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arr = array_map('strtoupper', $arr);
echo "<pre>";
print_r($arr);
echo "<pre>";

echo "<h2>9 Число в обратном направлении</h2>";

$num = 12345678;
echo $num;
echo "<br>";
$num = strrev($num);
echo $num;
echo "<hr>";
echo $num = 678;
if ($num == 678) {
    $b = $num % 10;
    $c = (($num - $b) / 10) % 10;
    $d = ($num - ($c * 10 + $b)) / 100;
    echo "<br>";
    echo $b * 100 + $c * 10 + $d;
}
echo "<br>";
echo $num = 5678;
echo "<br>";
if ($num == 5678) {
    $b = $num % 10;//8
    $c = (($num - $b) / 10) % 10;//7
    $d = ($num - ($c * 10 + $b)) / 100;//6
    $k = ($num - ($d * 10 + $c)) / 1000;
    echo "<br>";
    echo $b * 1000 + 1 * 100 + 1 * 10 + $k;
}
echo "<h2>10 Убывание</h2>";

$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
echo "<pre>";
print_r($arr);
echo "</pre>";

rsort($arr);


for ($x = 0; $x < count($arr); $x++) {
    echo $arr[$x] . " ";
}

echo "<pre>";
print_r($arr);
echo "</pre>";

//echo "<br>";
echo "****";
echo "<hr>";
include_once("form.php");



