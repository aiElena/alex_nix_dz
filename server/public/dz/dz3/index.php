<?php
session_start();
echo session_id();
echo "<br>";
echo "2bb85d94e89cbacaf80fc4a89d9e956c";
echo "<br>";
$id = 55;
$_SESSION['id'] = htmlspecialchars($id);
echo $_SESSION['id'];
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div>
    <h2>Оставить отзыв</h2>
    <form action="<?= $_SERVER['SCRIPT_NAME'] ?>" method="post">
        <input type='text' name="name" value="">
        <br>
        <textarea name='comment' cols='100' rows="10"></textarea>
        <br>
        <input type='submit' name="doGo" value="Go">
    </form>
</div>
<div>
    <h2>Добавить в корзину</h2>
    <form action="<?= $_SERVER['SCRIPT_NAME'] ?>" method="post">
        <input type='number' name="id" value="">
        <br>
        <input type='number' name="number" value="">
        <br>
        <input type='submit' name="doGo" value="Go">
    </form>
</div>
<div>
    <h2>Перезвонить клиенту</h2>
    <form action="<?= $_SERVER['SCRIPT_NAME'] ?>" method="post">
        <input type='text' name="name" value="">
        <br>
        <input type='tel' name="phone" value="">
        <br>
        <input type='submit' name="doGo" value="Go">
    </form>
</div>
<div>
    <h2>Оформить заказ</h2>
    <form action="<?= $_SERVER['SCRIPT_NAME'] ?>" method="post">
        <label for="name">Ваше имя</label>
        <input type='text' name="name" value="">
        <br>
        <label for="phone">Ваш телефон</label>
        <input type='tel' name="phone" value="">
        <br>
        <label for="delivery">Метод доставки</label>
        <input type='text' name="delivery" value="">
        <br>
        <label for="adress">Адресс доставки</label>
        <input type='text' name="adress" value="">
        <br>

        <input type='hidden' name="id" value="<?= $_SESSION['id'] ?>">

        <input type='submit' name="doGo" value="Go">
    </form>
</div>
<div>
    <h2>Загрузка файлов</h2>
    <form action="upload.php" method="post" enctype="multipart/form-data">
        <input type="file" name="image">
        <button type='submit'>Save</button>
    </form>
</div>

</body>
</html>


